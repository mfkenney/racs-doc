#+TITLE: RACS 0.0 OSX Network Configuration
#+STARTUP: showall
#+OPTIONS: toc:nil H:4 timestamp:nil author:nil
#+LATEX_CLASS: mfk-org-article
#+LATEX_CLASS_OPTIONS: [11pt,letterpaper]

* Selecting IP Address

The IP address for the camera is written on a piece of tape affixed to
the outside of the camera. It will have the form /10.N.M.X/ (probably
10.95.97.X). You will need to set the IP address of your laptop to
/10.N.M.Y/ (/Y/ != /X/).

* Network Setup

It's best to configure a new network "location" so as not to interfere with
your standard configuration.

** Open Network Preferences

[[file:Network_Prefs.png]]

** Add New Location

Click the Location pull-down menu at the top of the dialog box:

[[file:Locations.png]]

Click the plus button and the bottom of the list-box to add a new location,
name it "RACS0":

[[file:Add_New_Location.png]]

** Configure IP Address

After creating the new location, select it from the menu to make it
active, then select your Ethernet interface from the list of the
left-side of the dialog box.

Select /Manually/ from the *Configure IPv4* pull-down menu, enter the
IP address you determined earlier and set the *Subnet Mask* to
255.255.255.0.  Leave all other boxes blank and click the *Apply*
button at the bottom of the box.

[[file:Configure_Static_IP.png]]

** Accessing the Camera

Start your web-browser and enter the camera's IP address into the address box.
